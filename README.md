# Exchange Integration Sample Java Application

This is a sample Java application designed to showcase.
Please see Exchange iframe [documentation](https://gitlab.com/ruibarbosa-codigree/exchange-iframe-documentation)

The sample code is not intended to be used as-is in a production environment and carries no assurances or guarantees.

## Getting Started

### Prerequisites

Before you begin, ensure you have the following installed:
- Java SDK (17)

### Usage

Follow these steps to use the sample code:

1. Clone the repository:
    ```bash
    git clone git@gitlab.com:ruibarbosa-codigree/exchange-iframe-backend-example.git
    ```
   
2. Open the project in your preferred Java IDE (Eclipse, IntelliJ IDEA, etc.).
3. Change application.yml - put yours credentials and configure your DB.
4. Implement your security for your internal requests.

5. Explore the code and its functionality.

## Folder Structure

The endpoints Backend server to server communication are in package with name iframe.

## Support

For any questions or issues regarding this sample code, feel free to contact us.
