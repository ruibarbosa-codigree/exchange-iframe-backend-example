package com.cgwl.iframe.integration.api.entity;

import com.cgwl.iframe.integration.api.enums.AccountStatementOriginEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

import jakarta.persistence.UniqueConstraint;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;


import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Only for sample test.
 *
 */
@Entity
@Table(name = "account_statement", uniqueConstraints = {
        @UniqueConstraint(name = "UniqueStatementIdAndEntryDate", columnNames = {"statementId", "entryDate"})})
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AccountStatementEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Long statementId;

    @Column(nullable = false)
    private LocalDateTime entryDate;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "account_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private UserEntity userEntity;

    @Column
    private AccountStatementOriginEnum origin;

    @Column
    private BigDecimal debit;

    @Column
    private BigDecimal credit;

    @Column
    private String currency;

    @Column
    private String originId;

    @Column
    private String description;

    @Column
    private Double commissionRate;

    @Column(updatable = false, nullable = false)
    @CreationTimestamp
    private LocalDateTime createdAt;

    @UpdateTimestamp
    private LocalDateTime updatedAt;

}

