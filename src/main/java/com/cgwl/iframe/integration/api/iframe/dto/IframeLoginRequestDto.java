package com.cgwl.iframe.integration.api.iframe.dto;

import com.cgwl.iframe.integration.api.enums.AllowableCurrencyEnum;
import com.cgwl.iframe.integration.api.iframe.enums.AccountStatusEnum;
import com.cgwl.iframe.integration.api.iframe.enums.ClientLoginStatusEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
/**
 * Docs: https://gitlab.com/ruibarbosa-codigree/exchange-iframe-documentation
 * Required Object to authenticate in our side.
 * Please see Exchange iframe documentation 2.2.1 - Login
 * */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class IframeLoginRequestDto {
    private String accountId;
    private AllowableCurrencyEnum ccy;//allowed currencies had to be previously agreed.
    private BigDecimal position;//From 0 to 90 required only on user first login, then is optional
    private AccountStatusEnum accountStatus; // optional | If user can bet or not | (INACTIVE, ACTIVE)
    private ClientLoginStatusEnum clientLoginStatus; // optional | If user can login or not | (ON, OFF)
    private String positionRef;//optional

    public IframeLoginRequestDto(String accountId, AllowableCurrencyEnum ccy, BigDecimal position) {
        this.accountId = accountId;
        this.ccy = ccy;
        this.position = position;
    }
}
