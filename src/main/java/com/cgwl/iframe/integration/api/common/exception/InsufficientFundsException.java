package com.cgwl.iframe.integration.api.common.exception;

public class InsufficientFundsException extends RuntimeException{

    public InsufficientFundsException() {
        super("Insufficient funds");
    }
}
