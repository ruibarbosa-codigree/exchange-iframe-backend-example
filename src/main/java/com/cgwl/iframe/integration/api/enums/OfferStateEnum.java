package com.cgwl.iframe.integration.api.enums;

public enum OfferStateEnum {
    PENDING ,PLACED ,MATCHED, WON, LOST, CANCELED, LAPSED, VOIDED, EXPIRED
}
