package com.cgwl.iframe.integration.api.repository;

import com.cgwl.iframe.integration.api.entity.OfferEntity;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OffersRepository extends JpaRepository<OfferEntity, Long> {
    @Transactional
    void deleteByUserEntityAccountId(String accountId);

}
