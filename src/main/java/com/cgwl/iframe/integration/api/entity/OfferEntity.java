package com.cgwl.iframe.integration.api.entity;

import com.cgwl.iframe.integration.api.enums.BettingTypeEnum;
import com.cgwl.iframe.integration.api.enums.OfferStateEnum;
import com.cgwl.iframe.integration.api.iframe.dto.wagering.OfferRequestDto;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static jakarta.persistence.EnumType.STRING;

/**
 * Only for sample test.
 *
 */
@Entity
@Table(name = "offers", uniqueConstraints = {
        @UniqueConstraint(name = "UniqueIdAndStateAndLastUpdated", columnNames = {"id", "state", "lastUpdated"})})
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class OfferEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    @Enumerated(STRING)
    private OfferStateEnum state;

    @Column(nullable = false)
    private LocalDateTime lastUpdated;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "account_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private UserEntity userEntity;

    private Long betId;

    @Column(nullable = false)
    private BigDecimal virtualStake;

    private BigDecimal virtualStakeMatched;

    @Column(nullable = false)
    private BigDecimal odd;

    private BigDecimal averageOddMatched;

    @Column(nullable = false)
    private Boolean side;

    @Column(nullable = false)
    private Boolean inPlay;

    private String marketId;

    private String marketType;

    @Enumerated(STRING)
    @Column(nullable = false)
    private BettingTypeEnum bettingType;

    private LocalDateTime placedDate;

    private BigDecimal virtualProfit;

    @Column(nullable = false)
    private String eventId;

    @Column(nullable = false)
    private String eventName;

    private Long competitionId;

    private String competitionName;

    private Long eventTypeId;

    private String eventTypeName;

    private BigDecimal handicap;

    private String currency;

    private String marketName;

    private Long selectionId;

    private String selectionName;

    private String frontEndUrl;

    private BigDecimal realStake;

    private BigDecimal realStakeMatched;

    private BigDecimal realProfit;

    private LocalDateTime marketStartTime;

    private LocalDateTime settledDate;

    private String positionRef;

    @Column(updatable = false, nullable = false)
    @CreationTimestamp
    private LocalDateTime createdAt;

    @UpdateTimestamp
    private LocalDateTime updatedAt;

    public OfferEntity(UserEntity user, OfferRequestDto offerRequestDto) {
        this.userEntity = user;
        this.state = offerRequestDto.getState();
        this.lastUpdated = offerRequestDto.getLastUpdated();
        this.betId = offerRequestDto.getBetId();
        this.virtualStake = offerRequestDto.getVirtualStake();
        this.virtualStakeMatched = offerRequestDto.getVirtualStakeMatched();
        this.odd = offerRequestDto.getOdd();
        this.averageOddMatched = offerRequestDto.getAverageOddMatched();
        this.side = offerRequestDto.getSide();
        this.inPlay = offerRequestDto.getInPlay();
        this.marketId = offerRequestDto.getMarketId();
        this.marketType = offerRequestDto.getMarketType();
        this.bettingType = offerRequestDto.getBettingType();
        this.placedDate = offerRequestDto.getPlacedDate();
        this.virtualProfit = offerRequestDto.getVirtualProfit();
        this.eventId = offerRequestDto.getEventId();
        this.eventName = offerRequestDto.getEventName();
        this.competitionId = offerRequestDto.getCompetitionId();
        this.competitionName = offerRequestDto.getCompetitionName();
        this.eventTypeId = offerRequestDto.getEventTypeId();
        this.eventTypeName = offerRequestDto.getEventTypeName();
        this.handicap = offerRequestDto.getHandicap();
        this.currency = offerRequestDto.getCurrency();
        this.marketName = offerRequestDto.getMarketName();
        this.selectionId = offerRequestDto.getSelectionId();
        this.selectionName = offerRequestDto.getSelectionName();
        this.frontEndUrl = offerRequestDto.getFrontEndUrl();
        this.realStake = offerRequestDto.getRealStake();
        this.realStakeMatched = offerRequestDto.getRealStakeMatched();
        this.realProfit = offerRequestDto.getRealProfit();
        this.marketStartTime = offerRequestDto.getMarketStartTime();
        this.settledDate = offerRequestDto.getSettledDate();
        this.positionRef = offerRequestDto.getPositionRef();
    }
}
