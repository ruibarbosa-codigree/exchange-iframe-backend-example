package com.cgwl.iframe.integration.api.iframe.dto.wagering;

import com.cgwl.iframe.integration.api.iframe.enums.TransactionStatusEnum;
import com.cgwl.iframe.integration.api.iframe.enums.iFrameErrorResponseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TransactionResponseDto {

    private String accountId;
    private Long transactionId;
    private TransactionStatusEnum status;
    private BigDecimal availableBalance;// Not required
    private iFrameErrorResponseEnum errorCode; //Required only if status="ERROR"

    public TransactionResponseDto(BigDecimal availableBalance) {
        this.availableBalance = availableBalance;
    }
}
