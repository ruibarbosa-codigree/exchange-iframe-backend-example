package com.cgwl.iframe.integration.api.enums;

public enum AccountStatementOriginEnum {
    DEPOSIT, WITHDRAWAL, SETTLEMENT, CORRECTION, RESETTLEMENT, CHARGE_COMMISSION, CHARGE_TURNOVER, CHARGE_TRANSACTION
}
