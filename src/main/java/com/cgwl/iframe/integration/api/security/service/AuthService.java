package com.cgwl.iframe.integration.api.security.service;

import com.cgwl.iframe.integration.api.entity.UserEntity;
import com.cgwl.iframe.integration.api.repository.UserRepository;
import com.cgwl.iframe.integration.api.security.dto.request.LoginRequestDto;
import com.cgwl.iframe.integration.api.security.dto.response.LoginResponseDto;
import com.cgwl.iframe.integration.api.security.utils.JwtUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class AuthService {

    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    UserRepository userRepo;

    public Authentication authenticate (UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken) {
        return this.authenticationManager.authenticate(usernamePasswordAuthenticationToken);
    }

    public LoginResponseDto login (LoginRequestDto loginRequest) {
        log.info("[AuthService#login]");
        Authentication authentication = this.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        UserEntity userEntityDetails = (UserEntity) authentication.getPrincipal();
        String jwt = jwtUtils.generateJwtToken(userEntityDetails);
        return new LoginResponseDto(
                userEntityDetails.getFirstName(),
                userEntityDetails.getAvailableBalance(),
                userEntityDetails.getExposure(),
                userEntityDetails.getCurrency(),
                jwt);
    }

    public LoginResponseDto refreshToken(UserEntity userEntity) {
        log.info("[AuthService#refreshToken]");
        String jwt = jwtUtils.generateJwtToken(userEntity);
        return new LoginResponseDto(
                userEntity.getFirstName(),
                userEntity.getAvailableBalance(),
                userEntity.getExposure(),
                userEntity.getCurrency(),
                jwt);
    }
}
