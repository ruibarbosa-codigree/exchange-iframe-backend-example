package com.cgwl.iframe.integration.api.iframe.dto.wagering;

import com.cgwl.iframe.integration.api.enums.BettingTypeEnum;
import com.cgwl.iframe.integration.api.enums.OfferStateEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class OfferRequestDto {
    @NotNull
    @NotBlank
    private String accountId;
    private OfferStateEnum state;
    private Long betId;
    private BigDecimal virtualStake;
    private BigDecimal virtualStakeMatched;
    private BigDecimal odd;
    private BigDecimal averageOddMatched;
    private Boolean side;
    private Boolean inPlay;
    private String marketType;
    private BettingTypeEnum bettingType;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
    private LocalDateTime lastUpdated;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
    private LocalDateTime placedDate;
    private BigDecimal virtualProfit;
    private Long eventTypeId;
    private String eventTypeName;
    private Long competitionId;
    private String competitionName;
    private String eventId;
    private String eventName;
    private String marketId;
    private String marketName;
    private Long selectionId;
    private String selectionName;
    private BigDecimal handicap;
    @Pattern(regexp = "^[A-Z]{3}$")
    private String currency;
    private String frontEndUrl;
    private BigDecimal realStake;
    private BigDecimal realStakeMatched;
    private BigDecimal realProfit;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
    private LocalDateTime marketStartTime;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
    private LocalDateTime settledDate;
    private String positionRef;




}
