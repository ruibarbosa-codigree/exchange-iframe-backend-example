package com.cgwl.iframe.integration.api.enums;

/**
 * If you need more currencies please contact us.
 */
public enum AllowableCurrencyEnum {
    INR, HKD, USD, GBP, EUR, BRL
}
