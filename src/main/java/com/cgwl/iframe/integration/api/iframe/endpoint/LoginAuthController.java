package com.cgwl.iframe.integration.api.iframe.endpoint;

import com.cgwl.iframe.integration.api.entity.UserEntity;
import com.cgwl.iframe.integration.api.iframe.dto.IframeLoginResponseDto;
import com.cgwl.iframe.integration.api.iframe.service.LoginService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Slf4j
@RestController
@RequestMapping(
        path = "${application.web-root-path}/exchange",
        produces = APPLICATION_JSON_VALUE
)
public class LoginAuthController {
    @Autowired
    private LoginService loginService;

    /**
     * Example of internal endpoint to request login in exchange iframe
     * @param principal
     * @param request
     * @return exchange login token  be appended in the iframe SRC url within maximum 10 minutes of token creation.
     */
    @PostMapping("/login")
    public ResponseEntity<IframeLoginResponseDto> loginV2(@AuthenticationPrincipal UserDetails principal,
                                                          HttpServletRequest request){
        log.info("[LoginAuthController#/login]: exchange login");
        UserEntity userEntityDetails = (UserEntity) principal;
        return this.loginService.getLoginIframe(userEntityDetails, request);
    }

    @PutMapping("/updateUserPosition")
    public ResponseEntity<Void> updateUserPosition(@AuthenticationPrincipal UserDetails principal,
                                                          HttpServletRequest request){
        log.info("[LoginAuthController#updateClientPosition]: update the position taken");
        UserEntity userEntityDetails = (UserEntity) principal;
        return this.loginService.updateUserPosition(userEntityDetails, request);
    }
}
