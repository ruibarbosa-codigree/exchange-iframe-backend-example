package com.cgwl.iframe.integration.api.common.exception;

public class UserNotFoundException extends RuntimeException{
        static final String USER_NOT_FOUND_MESSAGE = "User Not Found";

        public UserNotFoundException() {super(USER_NOT_FOUND_MESSAGE);}

        public UserNotFoundException(String message) {super(message);}
}
