package com.cgwl.iframe.integration.api.iframe.enums;
/**
 * Required
 */
public enum iFrameErrorResponseEnum {
    INSUFFICIENT_FUNDS, ACCOUNT_BLOCKED, BETTING_NOT_ALLOWED, DUPLICATE_TRANSACTION
}
