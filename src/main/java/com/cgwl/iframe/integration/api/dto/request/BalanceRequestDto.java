package com.cgwl.iframe.integration.api.dto.request;

import com.cgwl.iframe.integration.api.enums.BalanceTypeEnum;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BalanceRequestDto {
    @NotNull
    private BalanceTypeEnum type;

    @NotNull
    private BigDecimal amount;

}
