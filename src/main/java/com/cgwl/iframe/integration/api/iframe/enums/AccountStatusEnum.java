package com.cgwl.iframe.integration.api.iframe.enums;

public enum AccountStatusEnum {
    ACTIVE, INACTIVE
}
