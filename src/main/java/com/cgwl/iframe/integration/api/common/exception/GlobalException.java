package com.cgwl.iframe.integration.api.common.exception;

public class GlobalException extends RuntimeException{
    static final String GLOBAL_EXCEPTION_MESSAGE = "Something Went Wrong";

    public GlobalException() {super(GLOBAL_EXCEPTION_MESSAGE);}

    public GlobalException(String message) {super(message);}
}
