package com.cgwl.iframe.integration.api.security.endpoint;

import com.cgwl.iframe.integration.api.common.exception.InvalidCredentialsException;
import com.cgwl.iframe.integration.api.entity.UserEntity;
import com.cgwl.iframe.integration.api.security.dto.request.LoginRequestDto;
import com.cgwl.iframe.integration.api.security.dto.response.LoginResponseDto;
import com.cgwl.iframe.integration.api.security.service.AuthService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * This controller is used only for authentication.
 * This is basic authentication. We recommended to implement one security method more secure
 * Please implement yours endpoints as needed
 */
@Slf4j
@RestController
@RequestMapping(
        path = "${application.web-root-path}/auth",
        produces = APPLICATION_JSON_VALUE
)
public class AuthRestController {

    public static final String LOGIN_PATH = "/login";

    @Autowired
    private AuthService authService;

    @PostMapping(LOGIN_PATH)
    public ResponseEntity<LoginResponseDto> login(@Valid @RequestBody LoginRequestDto loginRequestDto) {
        log.info("[AuthRestController#login]: request to process {}.", loginRequestDto);
    return ResponseEntity.ok(this.authService.login(loginRequestDto));
    }

    @PostMapping ("/refresh-token")
    public ResponseEntity<LoginResponseDto> refreshToken(@AuthenticationPrincipal UserDetails principal) {
        if (principal == null) {
            throw new InvalidCredentialsException();
        }
        UserEntity userEntityDetails = (UserEntity) principal;
        log.info("[AuthRestController#refresh-token]: request to process {}.", userEntityDetails);

        return ResponseEntity.ok(this.authService.refreshToken(userEntityDetails));
    }

}
