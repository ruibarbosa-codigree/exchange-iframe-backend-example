package com.cgwl.iframe.integration.api.iframe.enums;

/**
 * Required
 */
public enum TransactionTypeEnum {
    CREDIT, DEBIT, DEBIT_ALLOW
}
