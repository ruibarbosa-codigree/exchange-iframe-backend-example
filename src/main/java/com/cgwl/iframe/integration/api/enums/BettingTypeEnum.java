package com.cgwl.iframe.integration.api.enums;

public enum BettingTypeEnum {
    ODDS, LINE, ASIAN_HANDICAP_DOUBLE_LINE, ASIAN_HANDICAP_SINGLE_LINE
}
