package com.cgwl.iframe.integration.api.iframe.dto;

import com.cgwl.iframe.integration.api.iframe.enums.AccountStatusEnum;
import com.cgwl.iframe.integration.api.iframe.enums.ClientLoginStatusEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

/**
 * Docs: https://gitlab.com/ruibarbosa-codigree/exchange-iframe-documentation
 * Required Object to update the position taken of a user that is already logged in.
 * Please see Exchange iframe documentation 2.2.3 - Update user position
 * */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class IframeUpdateClientPositionRequestDto {
    private String accountId;
    private BigDecimal position;//From 0 to 90
    private AccountStatusEnum accountStatus; // optional | If user can bet or not | (INACTIVE, ACTIVE)
    private ClientLoginStatusEnum clientLoginStatus; // optional | If user can login or not | (ON, OFF)
    private String positionRef;//optional

    public IframeUpdateClientPositionRequestDto(String accountId, BigDecimal position) {
        this.accountId = accountId;
        this.position = position;
    }
}
