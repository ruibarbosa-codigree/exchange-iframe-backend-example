package com.cgwl.iframe.integration.api.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserBalanceResponseDto {

    private BigDecimal availableBalance;
    private BigDecimal exposure;
}
