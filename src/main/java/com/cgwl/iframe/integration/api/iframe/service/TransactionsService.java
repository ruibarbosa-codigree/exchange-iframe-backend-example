package com.cgwl.iframe.integration.api.iframe.service;

import com.cgwl.iframe.integration.api.common.exception.GlobalException;
import com.cgwl.iframe.integration.api.common.exception.UserNotFoundException;
import com.cgwl.iframe.integration.api.entity.TransactionEntity;
import com.cgwl.iframe.integration.api.entity.UserEntity;
import com.cgwl.iframe.integration.api.iframe.dto.wagering.TransactionRequestDto;
import com.cgwl.iframe.integration.api.iframe.dto.wagering.TransactionResponseDto;
import com.cgwl.iframe.integration.api.iframe.enums.TransactionStatusEnum;
import com.cgwl.iframe.integration.api.iframe.enums.TransactionTypeEnum;
import com.cgwl.iframe.integration.api.iframe.enums.iFrameErrorResponseEnum;
import com.cgwl.iframe.integration.api.repository.TransactionsRepository;
import com.cgwl.iframe.integration.api.repository.UserRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class TransactionsService {
    @Autowired
    TransactionsRepository transactionsRepo;

    @Autowired
    UserRepository userRepo;

    @Transactional
    public ResponseEntity<TransactionResponseDto> updateBalance(TransactionRequestDto request) {
        UserEntity userEntity = userRepo.findByAccountId(request.getAccountId())
                .orElseThrow(() -> new UserNotFoundException("User not Found with Account ID: "
                        + request.getAccountId()));

        TransactionResponseDto transactionResponseDto = new TransactionResponseDto(userEntity.getAccountId(),
                request.getTransactionId(),
                TransactionStatusEnum.ERROR,
                userEntity.getAvailableBalance(),
                iFrameErrorResponseEnum.INSUFFICIENT_FUNDS);
        try{


            if (!checkDebitPermission(userEntity, request.getAmount(), request.getType())) {
                return ResponseEntity.status(HttpStatus.OK).body(transactionResponseDto);
            }

            TransactionEntity transactionEntity =
                    transactionsRepo.findByReqTransactionId(request.getTransactionId());

            if (request.getWasRolledBack()) {
                if (transactionEntity == null) {
                    throw new GlobalException("Transaction with ID: " + request.getTransactionId() + " Was Not Found");
                }

                this.updateUserAvailableBalance(userEntity, request.getAmount());
                if (request.getType().equals(TransactionTypeEnum.DEBIT) && userEntity.getAvailableBalance().compareTo(BigDecimal.ZERO) < 0) {
                    return ResponseEntity.status(HttpStatus.OK).body(transactionResponseDto);
                }

                transactionEntity.setAmount(request.getAmount());
                transactionEntity.setLastUpdated(request.getLastUpdated());
                transactionEntity.setType(request.getType());
                transactionEntity.setWasRolledBack(request.getWasRolledBack());

                transactionsRepo.save(transactionEntity);
                userRepo.save(userEntity);
                //TODO: verify if transaction was not successful, if it fails it shouldn't update user
                transactionResponseDto.setStatus(TransactionStatusEnum.OK);
                transactionResponseDto.setErrorCode(null);
                transactionResponseDto.setAvailableBalance(userEntity.getAvailableBalance());

                return ResponseEntity.status(HttpStatus.OK).body(transactionResponseDto);
            }
            //Check if duplicated transaction
            if (transactionEntity != null) {
                transactionResponseDto.setErrorCode(iFrameErrorResponseEnum.DUPLICATE_TRANSACTION);
                return ResponseEntity.status(HttpStatus.OK).body(transactionResponseDto);
            }
            //Process valid transaction
            userEntity.setAvailableBalance(userEntity.getAvailableBalance().add(request.getAmount()));
            this.createTransaction(request, userEntity);
            userRepo.save(userEntity);

            transactionResponseDto.setStatus(TransactionStatusEnum.OK);
            transactionResponseDto.setErrorCode(null);
            transactionResponseDto.setAvailableBalance(userEntity.getAvailableBalance());

        }
        catch (Exception ex){
            throw new GlobalException("Possible cause: Duplicate Transaction ID ");
        }
        return ResponseEntity.ok(transactionResponseDto);

    }

    private void createTransaction(TransactionRequestDto request, UserEntity userEntity) {
        TransactionEntity transactionEntity = new TransactionEntity();
        transactionEntity.setUserEntity(userEntity);
        transactionEntity.setReqTransactionId(request.getTransactionId());
        transactionEntity.setType(request.getType());
        transactionEntity.setMarketId(request.getMarketId());
        transactionEntity.setLastUpdated(request.getLastUpdated());
        transactionEntity.setAmount(request.getAmount());
        transactionEntity.setDescription(request.getDescription());
        transactionEntity.setWasRolledBack(request.getWasRolledBack());
        this.transactionsRepo.save(transactionEntity);
    }

    public Boolean checkDebitPermission(UserEntity userEntity, BigDecimal amount, TransactionTypeEnum type) {
        if (type.equals(TransactionTypeEnum.DEBIT)) {
            return amount.abs().compareTo(userEntity.getAvailableBalance()) <= 0;
        }
        return true;
    }

    private void updateUserAvailableBalance(UserEntity userEntity, BigDecimal amount) {
        userEntity.setAvailableBalance(userEntity.getAvailableBalance().add(amount));
    }
}
