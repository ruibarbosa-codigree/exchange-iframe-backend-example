package com.cgwl.iframe.integration.api.endpoint;

import com.cgwl.iframe.integration.api.dto.request.BalanceRequestDto;
import com.cgwl.iframe.integration.api.dto.response.UserBalanceResponseDto;
import com.cgwl.iframe.integration.api.entity.UserEntity;
import com.cgwl.iframe.integration.api.service.UserService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * This controller is used only for sample.
 * Please implement your endpoints as needed
 */
@Slf4j
@RestController
@RequestMapping(
        path = "${application.web-root-path}/user",
        produces = APPLICATION_JSON_VALUE
)
public class UserRestController {

    @Autowired
    UserService userService;
    @PutMapping("/transfer")
    public ResponseEntity<UserBalanceResponseDto> updateBalance(
            @Valid @RequestBody BalanceRequestDto requestDto,
            @AuthenticationPrincipal UserDetails principal
    ) {
        log.info("[UserRestController#updateBalance]: transferType to process {}.", requestDto);
        UserEntity userEntityDetails = (UserEntity) principal;
        return ResponseEntity.ok(this.userService.updateUserBalance(requestDto, userEntityDetails));
    }

    @GetMapping("/balance")
    public ResponseEntity<UserBalanceResponseDto> getUserBalance(@AuthenticationPrincipal UserDetails principal) {
        UserEntity userEntityDetails = (UserEntity) principal;
        log.info("[UserRestController#balance]: request to process {}.", userEntityDetails);
        return ResponseEntity.ok(this.userService.getUserBalance(userEntityDetails));

    }
}
