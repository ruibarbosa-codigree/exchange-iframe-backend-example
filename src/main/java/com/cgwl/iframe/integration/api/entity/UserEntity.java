package com.cgwl.iframe.integration.api.entity;

import com.cgwl.iframe.integration.api.enums.AllowableCurrencyEnum;
import com.cgwl.iframe.integration.api.enums.AllowableLangEnum;
import com.cgwl.iframe.integration.api.enums.RoleEnum;
import jakarta.persistence.*;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

import static java.math.BigDecimal.ZERO;

/**
 * Only for sample test.
 *
 */
@Entity
@Table(name = "users")
@NoArgsConstructor
@Getter
@Setter
public class UserEntity implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true, nullable = false)
    @Size(min = 6)
    private String username;

    @Column(nullable = false)
    @Size(min = 2)
    private String firstName;

    @Column(nullable = false)
    @Size(min = 2)
    private String lastName;

    @Column(unique = true, nullable = false)
    @Pattern(regexp = "^(?=.{6,50}$)([a-zA-Z0-9-_|]+)$")
    private String accountId;

    @Column(nullable = false)
    @Size(min = 6)
    private String password;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private RoleEnum role;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private AllowableCurrencyEnum currency;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private AllowableLangEnum lang;

    @Column(nullable = false)
    private BigDecimal availableBalance = ZERO;

    private BigDecimal exposure = ZERO;
    @Column(updatable = false, nullable = false)
    @CreationTimestamp
    private LocalDateTime createdAt;

    @UpdateTimestamp
    private LocalDateTime updatedAt;

    private LocalDateTime lastExchangeLogin;

    public UserEntity(String username, String password, String firstName, String lastName) {
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.role = RoleEnum.USER;
        this.currency = AllowableCurrencyEnum.BRL;
        this.lang = AllowableLangEnum.EN;
        this.createdAt = LocalDateTime.now();
        this.accountId = username;
        this.availableBalance = BigDecimal.valueOf(100);
    }

    public UserEntity(String username,
                      String password,
                      String firstName,
                      String lastName,
                      RoleEnum role,
                      AllowableCurrencyEnum currency,
                      AllowableLangEnum lang) {
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.role = role;
        this.currency = currency;
        this.lang = lang;
        this.createdAt = LocalDateTime.now();
        this.accountId = username + "_";
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(new SimpleGrantedAuthority("ROLE_" + role.name()));
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
