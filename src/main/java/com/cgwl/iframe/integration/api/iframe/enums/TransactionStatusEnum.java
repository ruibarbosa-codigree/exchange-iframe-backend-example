package com.cgwl.iframe.integration.api.iframe.enums;
/**
 * Required
 */
public enum TransactionStatusEnum {
    OK, ERROR
}
