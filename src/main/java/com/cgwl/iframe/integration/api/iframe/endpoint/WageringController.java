package com.cgwl.iframe.integration.api.iframe.endpoint;


import com.cgwl.iframe.integration.api.iframe.dto.wagering.AccountStatementRequestDto;
import com.cgwl.iframe.integration.api.iframe.dto.wagering.OfferRequestDto;
import com.cgwl.iframe.integration.api.iframe.dto.wagering.TransactionRequestDto;
import com.cgwl.iframe.integration.api.iframe.dto.wagering.TransactionResponseDto;
import com.cgwl.iframe.integration.api.iframe.service.AccountStatementService;
import com.cgwl.iframe.integration.api.iframe.service.OffersService;
import com.cgwl.iframe.integration.api.iframe.service.TransactionsService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * Example of controller to process supplied APIs
 */
@Slf4j
@RestController
@RequestMapping(
        path = "${application.web-root-path}/wagering",
        produces = APPLICATION_JSON_VALUE
)
public class WageringController {

    @Autowired
    TransactionsService transactionsService;

    @Autowired
    OffersService offersService;

    @Autowired
    AccountStatementService accountStatementService;

    /**
     * Docs: https://gitlab.com/ruibarbosa-codigree/exchange-iframe-documentation
     * Required - We will call this endpoint every time there is a balance update to be made on a user.
     * Please see Exchange iframe documentation 2.3.1 - Transfer
     * @param request TransactionRequestDto
     * @return ResponseEntity - TransactionResponseDto
     */
    @PostMapping("/transfer")
    public ResponseEntity<TransactionResponseDto> balanceInfo(@RequestBody TransactionRequestDto request){
        log.info("[WageringController#/transfer]: transfer toProcess {}", request);

        return transactionsService.updateBalance(request);
    }

    /**
     * Docs: https://gitlab.com/ruibarbosa-codigree/exchange-iframe-documentation
     * Required - This is only a report export, to give you full details of betting activity. This must not change user balance.
     * Please see Exchange iframe documentation 2.3.2 - Offers
     * @param offerRequest List<OfferRequestDto>
     * @return ResponseEntity Void
     */
    @PostMapping("/offers")
    public ResponseEntity<Void> updateOffers(@Valid @RequestBody List<OfferRequestDto> offerRequest) {
        log.info("[WageringController#offers]: offers toProcess {}", offerRequest);

        this.offersService.updateUsersOffer(offerRequest);
        return ResponseEntity.ok().build();
    }

    /**
     * Docs: https://gitlab.com/ruibarbosa-codigree/exchange-iframe-documentation
     * Required - This is only a report export, to give you full details of user balance changes done by the transfer endpoint. This must not change user balance.
     * Please see Exchange iframe documentation 2.3.3 - Account statement
     * @param accountStatementRequestDto List<AccountStatementRequestDto>
     * @return ResponseEntity Void
     */
    @PostMapping("/statements")
    public ResponseEntity<Void> updateAccountStatement(
            @RequestBody List<AccountStatementRequestDto> accountStatementRequestDto) {
        log.info("[WageringController#statements]: statements toProcess {}", accountStatementRequestDto);

        this.accountStatementService.updateUsersAccountStatement(accountStatementRequestDto);
        return ResponseEntity.ok().build();
    }
}
