package com.cgwl.iframe.integration.api.enums;

public enum BalanceTypeEnum {
    DEPOSIT,
    WITHDRAW
}
