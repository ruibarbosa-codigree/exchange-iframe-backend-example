package com.cgwl.iframe.integration.api.iframe.service;

import com.cgwl.iframe.integration.api.entity.AccountStatementEntity;
import com.cgwl.iframe.integration.api.entity.UserEntity;
import com.cgwl.iframe.integration.api.iframe.dto.wagering.AccountStatementRequestDto;
import com.cgwl.iframe.integration.api.repository.AccountStatementRepository;
import com.cgwl.iframe.integration.api.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class AccountStatementService {

    @Autowired
    AccountStatementRepository accountStatementRepo;
    @Autowired
    UserRepository userRepo;

    public void updateUsersAccountStatement(List<AccountStatementRequestDto> accountStatementRequestDtoList) {
        // IMPORTANT: This is only a sample! You can implement it your way;
        Set<String> accountIds = new HashSet<>();
        accountStatementRequestDtoList.forEach(accountStatementRequestDto -> {
            accountIds.add(accountStatementRequestDto.getAccountId());
        });

        List<UserEntity> users = this.userRepo.findAllByAccountId(accountIds);
        HashMap<String, UserEntity> userByAccountId = new HashMap<>();
        users.forEach(userEntity -> {
            if(!userByAccountId.containsKey(userEntity.getAccountId())) {
                userByAccountId.put(userEntity.getAccountId(),userEntity);
            }
        });

        List<AccountStatementEntity> accountStatementsToSave = new ArrayList<>();
        accountStatementRequestDtoList.forEach(accountStatementRequestDto -> {
            AccountStatementEntity accountStatement =
                    this.generateAccountStatement(
                            userByAccountId.get(accountStatementRequestDto.getAccountId()),
                            accountStatementRequestDto);
            accountStatementsToSave.add(accountStatement);
        });

        this.accountStatementRepo.saveAll(accountStatementsToSave);
    }

    private AccountStatementEntity generateAccountStatement(
            UserEntity user, AccountStatementRequestDto accountStatementRequestDto) {
        // IMPORTANT: This is only a sample you can implement it your way;
        AccountStatementEntity accountStatement = new AccountStatementEntity();
        accountStatement.setUserEntity(user);
        accountStatement.setStatementId(accountStatementRequestDto.getStatementId());
        accountStatement.setEntryDate(accountStatementRequestDto.getEntryDate());
        accountStatement.setOrigin(accountStatementRequestDto.getOrigin());
        accountStatement.setDebit(accountStatementRequestDto.getDebit());
        accountStatement.setCredit(accountStatementRequestDto.getCredit());
        accountStatement.setCurrency(accountStatementRequestDto.getCurrency());
        accountStatement.setOriginId(accountStatementRequestDto.getOriginId());
        accountStatement.setDescription(accountStatementRequestDto.getDescription());
        accountStatement.setCommissionRate(accountStatementRequestDto.getCommissionRate());

        return accountStatement;
    }
}
