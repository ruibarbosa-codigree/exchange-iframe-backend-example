package com.cgwl.iframe.integration.api.iframe.service;

import com.cgwl.iframe.integration.api.common.exception.GlobalException;
import com.cgwl.iframe.integration.api.common.exception.InvalidCredentialsException;
import com.cgwl.iframe.integration.api.entity.UserEntity;
import com.cgwl.iframe.integration.api.iframe.dto.IframeLoginRequestDto;
import com.cgwl.iframe.integration.api.iframe.dto.IframeLoginResponseDto;

import com.cgwl.iframe.integration.api.iframe.dto.IframeUpdateClientPositionRequestDto;
import com.cgwl.iframe.integration.api.repository.UserRepository;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Slf4j
@Service
public class LoginService {
    @Autowired
    RestTemplate restTemplate;
    @Autowired
    UserRepository userRepo;
    @Value("${application.user-agent}")
    private String userAgentName; // Provided as an integration credential
    @Value("${application.agent-token}")
    private String agentToken; // Provided as an integration credential
    @Value("${application.exchange-api-url}")
    private String exchange_api_url;

    /**
     * Docs: https://gitlab.com/ruibarbosa-codigree/exchange-iframe-documentation
     * Required  authenticate in our side.
     * Please see Exchange iframe documentation 2.2.1 - Login
     *
     * @param userEntity your user
     * @param request    only to get parentUrl
     * @return This will return a token identifying that user in our system, that should be appended in the iframe SRC url within maximum 10 minutes of token creation.
     */
    public ResponseEntity<IframeLoginResponseDto> getLoginIframe(UserEntity userEntity,
                                                                 HttpServletRequest request) {
        log.info("[LoginService#getExchangeLoginIframe]");

        HttpHeaders headers = new HttpHeaders();
        headers.set("agentUserName", this.userAgentName);
        headers.set("agentToken  ", this.agentToken);
        headers.set("parentUrl", request.getHeader("origin"));
        headers.set("Content-Type", "application/json");
        //Note: Position value above is only for sample purpose
        IframeLoginRequestDto loginRequest =
                new IframeLoginRequestDto(
                        userEntity.getAccountId(),
                        userEntity.getCurrency(),
                        new BigDecimal(20));
        String loginUrl = this.exchange_api_url + "/loginV2";
        HttpEntity<IframeLoginRequestDto> requestHttpEntity = new HttpEntity<>(loginRequest, headers);
        ResponseEntity<IframeLoginResponseDto> result;
        try {
            result = this.restTemplate.exchange(
                    loginUrl,
                    HttpMethod.POST,
                    requestHttpEntity,
                    IframeLoginResponseDto.class
            );

        } catch (Exception ex) {
            throw new InvalidCredentialsException("Something happen calling iframe login: " + ex.getMessage());
        }


        if (result.getBody() != null && result.getStatusCode().equals(HttpStatus.OK)) {
            userEntity.setLastExchangeLogin(LocalDateTime.now());
            this.userRepo.save(userEntity);
            return result;
        } else {
            throw new InvalidCredentialsException("Something happen calling iframe login: " + result.getBody());
        }
    }

    /**
     * Docs: https://gitlab.com/ruibarbosa-codigree/exchange-iframe-documentation
     * Request to update the position taken of a user that is already logged in
     * Please see Exchange iframe documentation 2.2.3 - Update user position
     *
     * @param userEntity your user
     * @param request    only to get parentUrl
     * @return If success only return http status code 200
     */
    public ResponseEntity<Void> updateUserPosition(UserEntity userEntity,
                                                   HttpServletRequest request) {
        log.info("[LoginService#updateUserPosition]");

        HttpHeaders headers = new HttpHeaders();
        headers.set("agentUserName", this.userAgentName);
        headers.set("agentToken  ", this.agentToken);
        headers.set("parentUrl", request.getHeader("origin"));
        headers.set("Content-Type", "application/json");
        IframeUpdateClientPositionRequestDto iframeUpdateClientPositionRequestDto =
                new IframeUpdateClientPositionRequestDto(userEntity.getAccountId(), new BigDecimal(30));
        String loginUrl = this.exchange_api_url + "/updateClientPosition";
        HttpEntity<IframeUpdateClientPositionRequestDto> requestHttpEntity =
                new HttpEntity<>(iframeUpdateClientPositionRequestDto, headers);
        ResponseEntity<Void> result;
        try {
            result = this.restTemplate.exchange(
                    loginUrl,
                    HttpMethod.POST,
                    requestHttpEntity,
                    Void.class
            );

        } catch (Exception ex) {
            // You need to handle with exceptions. Example!
            throw new GlobalException("Something happen calling iframe login: " + ex.getMessage());
        }
        return result;

    }


}
