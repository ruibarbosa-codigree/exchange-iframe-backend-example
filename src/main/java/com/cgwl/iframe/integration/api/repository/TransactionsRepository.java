package com.cgwl.iframe.integration.api.repository;

import com.cgwl.iframe.integration.api.entity.TransactionEntity;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface TransactionsRepository extends JpaRepository<TransactionEntity, Long> {

    @Transactional
    void deleteByUserEntityAccountId(String accountId);

    TransactionEntity findByReqTransactionId(Long reqTransactionId);
}
