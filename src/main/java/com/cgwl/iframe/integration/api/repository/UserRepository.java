package com.cgwl.iframe.integration.api.repository;

import com.cgwl.iframe.integration.api.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface UserRepository extends JpaRepository<UserEntity, Long> {

    Optional<UserEntity> findByAccountId(String accountId);
    Optional<UserEntity> findByUsername(String username);

    @Query("Select U from UserEntity U where accountId in :accountIds")
    List<UserEntity> findAllByAccountId(@Param("accountIds")Set<String> accountIds);

    Boolean existsByUsername(String username);
}
