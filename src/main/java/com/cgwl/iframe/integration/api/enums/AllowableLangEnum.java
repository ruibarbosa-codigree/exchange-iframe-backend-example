package com.cgwl.iframe.integration.api.enums;

public enum AllowableLangEnum {
    EN, PT_BR
}
