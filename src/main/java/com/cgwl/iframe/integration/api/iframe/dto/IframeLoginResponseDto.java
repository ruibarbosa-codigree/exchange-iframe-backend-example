package com.cgwl.iframe.integration.api.iframe.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
/**
 * Docs: https://gitlab.com/ruibarbosa-codigree/exchange-iframe-documentation
 * Response object
 * Please see Exchange iframe documentation 2.2.1 - Login (response)
 * */
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class IframeLoginResponseDto {

    private String baseUrl;
    private String token;
}
