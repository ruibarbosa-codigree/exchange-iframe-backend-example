package com.cgwl.iframe.integration.api.iframe.dto.wagering;

import com.cgwl.iframe.integration.api.iframe.enums.TransactionTypeEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TransactionRequestDto {
    private String accountId;
    private TransactionTypeEnum type;
    private String marketId;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
    private LocalDateTime lastUpdated;
    private Long transactionId;
    private BigDecimal amount;
    private String description;
    private Boolean wasRolledBack; //Can be true only for DEBIT, DEBIT_ALLOW transactions
    private String frontEndUrl;
}
