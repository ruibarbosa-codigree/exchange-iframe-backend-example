package com.cgwl.iframe.integration.api.common.controller;

import com.cgwl.iframe.integration.api.common.exception.GlobalException;
import com.cgwl.iframe.integration.api.common.exception.InsufficientFundsException;
import com.cgwl.iframe.integration.api.common.exception.InvalidCredentialsException;
import com.cgwl.iframe.integration.api.common.exception.UserNotFoundException;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.ResponseEntity.status;

/**
 * Example of exception handling.
 *
 * Please note that is just a demonstration example
 */
@ControllerAdvice
@Slf4j
public class ExceptionControllerAdvice {

    @ExceptionHandler(InsufficientFundsException.class)
    public ResponseEntity<String> handleInsufficientFundsException(InsufficientFundsException exception) {
        log.debug(
                "Insufficient funds exception resolved as {} for {} : {} ",
                FORBIDDEN, exception.getClass(), exception.getMessage()
        );
        return status(FORBIDDEN).body(exception.getMessage());
    }

    @ExceptionHandler(InvalidCredentialsException.class)
    public ResponseEntity<String> handleInvalidCredentialsException(InvalidCredentialsException exception) {
        log.debug(
                "Invalid credentials resolved as {} for {} : {} ",
                FORBIDDEN, exception.getClass(), exception.getMessage()
        );
        return status(FORBIDDEN).body(exception.getMessage());
    }

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<String> handleUserNotFoundException(UserNotFoundException exception) {
        log.debug(
                "User Not Found resolved as {} for {} : {} ",
                NOT_FOUND, exception.getClass(), exception.getMessage()
        );
        return status(NOT_FOUND).body(exception.getMessage());
    }

    @ExceptionHandler(GlobalException.class)
    public ResponseEntity<String> handleGlobalException(GlobalException exception) {
        log.debug(
                "Something went wrong resolved as {} for {} : {} ",
                CONFLICT, exception.getClass(), exception.getMessage()
        );
        return status(CONFLICT).body(exception.getMessage());
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    protected ResponseEntity<String> handleDataIntegrityViolation(DataIntegrityViolationException exception) {
        log.debug(
                "Something went wrong in Database resolved as {} for {} : {} ",
                CONFLICT, exception.getClass(), exception.getMessage()
        );
        return status(CONFLICT).body(exception.getMessage());
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<String> handleException(HttpMessageNotReadableException exception, HttpServletRequest request) {
        log.debug(
                "Something went wrong in Database resolved as {} for {} in requests {} : {} ",
                CONFLICT, exception.getClass(), request, exception.getMessage()
        );
        return status(CONFLICT).body(exception.getMessage());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    protected ResponseEntity<?> handleMethodArgumentNotValid
            (HttpServletRequest request, MethodArgumentNotValidException exception) {
        HashMap<String, List<String>> fieldErrors = new HashMap<>();
        exception.getBindingResult().getFieldErrors().forEach(error -> {
            if (fieldErrors.containsKey(error.getField())) {
                fieldErrors.get(error.getField()).add(error.getDefaultMessage());
            } else {
                fieldErrors.put(error.getField(), new ArrayList<>());
                fieldErrors.get(error.getField()).add(error.getDefaultMessage());
            }
        });
        return status(BAD_REQUEST).body(fieldErrors);
    }
}
