package com.cgwl.iframe.integration.api.common.exception;

public class InvalidCredentialsException extends RuntimeException{
    static final String INVALID_CREDENTIALS_MESSAGE = "Invalid credentials";

    public InvalidCredentialsException () {
        super(INVALID_CREDENTIALS_MESSAGE);
    }

    public InvalidCredentialsException(String message) {
        super(message);
    }
}
