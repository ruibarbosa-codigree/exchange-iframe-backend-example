package com.cgwl.iframe.integration.api.security.dto.response;

import com.cgwl.iframe.integration.api.enums.AllowableCurrencyEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class LoginResponseDto {
    private String firstName;
    private BigDecimal availableBalance;
    private BigDecimal exposure;
    private AllowableCurrencyEnum currency;
    private String token;
    private String type = "Bearer";

    public LoginResponseDto(
            String firstName,
            BigDecimal availableBalance,
            BigDecimal exposure,
            AllowableCurrencyEnum currency,
            String token) {
        this.firstName = firstName;
        this.availableBalance = availableBalance;
        this.exposure = exposure;
        this.token = token;
        this.currency = currency;
    }
}
