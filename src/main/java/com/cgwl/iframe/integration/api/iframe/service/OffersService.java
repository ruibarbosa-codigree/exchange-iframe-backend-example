package com.cgwl.iframe.integration.api.iframe.service;

import com.cgwl.iframe.integration.api.entity.OfferEntity;
import com.cgwl.iframe.integration.api.entity.UserEntity;
import com.cgwl.iframe.integration.api.iframe.dto.wagering.OfferRequestDto;
import com.cgwl.iframe.integration.api.repository.OffersRepository;
import com.cgwl.iframe.integration.api.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Service
public class OffersService {

    @Autowired
    OffersRepository offersRepo;
    @Autowired
    UserRepository userRepo;

    public void updateUsersOffer(List<OfferRequestDto> offerRequestDtoList) {
        // IMPORTANT: This is only a sample! You can implement it your way;
        Set<String> accountIds = new HashSet<>();
        offerRequestDtoList.forEach(offerRequestDto -> {
            accountIds.add(offerRequestDto.getAccountId());
        });

        List<UserEntity> users = this.userRepo.findAllByAccountId(accountIds);
        HashMap<String, UserEntity> userByAccountId = new HashMap<>();
        users.forEach(userEntity -> {
            if (!userByAccountId.containsKey(userEntity.getAccountId())) {
                userByAccountId.put(userEntity.getAccountId(), userEntity);
            }
        });

        List<OfferEntity> offerEntitiesToSave = new ArrayList<>();
        offerRequestDtoList.forEach(offerRequestDto -> {

            OfferEntity offerEntity =
                    this.generateOffer(userByAccountId.get(offerRequestDto.getAccountId()), offerRequestDto);
            offerEntitiesToSave.add(offerEntity);
        });

        this.offersRepo.saveAll(offerEntitiesToSave);
    }

    private OfferEntity generateOffer(UserEntity user, OfferRequestDto offerRequestDto) {

        return new OfferEntity(user, offerRequestDto);
    }
}
