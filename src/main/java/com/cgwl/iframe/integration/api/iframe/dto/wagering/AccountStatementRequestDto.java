package com.cgwl.iframe.integration.api.iframe.dto.wagering;

import com.cgwl.iframe.integration.api.enums.AccountStatementOriginEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AccountStatementRequestDto {
    private Long statementId;
    private String accountId;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
    private LocalDateTime entryDate;
    private AccountStatementOriginEnum origin;
    private String description;
    private String originId;
    private BigDecimal debit;
    private BigDecimal credit;
    private String currency;
    private Double commissionRate;//Required only if origin= "CHARGE_COMMISSION"
}
