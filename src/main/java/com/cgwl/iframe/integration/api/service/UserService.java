package com.cgwl.iframe.integration.api.service;

import com.cgwl.iframe.integration.api.common.exception.InsufficientFundsException;
import com.cgwl.iframe.integration.api.dto.request.BalanceRequestDto;
import com.cgwl.iframe.integration.api.dto.response.UserBalanceResponseDto;
import com.cgwl.iframe.integration.api.entity.UserEntity;
import com.cgwl.iframe.integration.api.enums.BalanceTypeEnum;
import com.cgwl.iframe.integration.api.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Slf4j
@Service
public class UserService {

    @Autowired
    UserRepository userRepo;
    public UserBalanceResponseDto updateUserBalance(
            BalanceRequestDto balanceRequestDto, UserEntity userEntity) {
        log.info("[UserService#updateUserBalance]");
        BigDecimal amountValue =
                balanceRequestDto.getType().equals(BalanceTypeEnum.WITHDRAW) ?
                        balanceRequestDto.getAmount().negate() : balanceRequestDto.getAmount();
        if (balanceRequestDto.getType().equals(BalanceTypeEnum.WITHDRAW)) {
            this.checkWithdrawPermission(userEntity,balanceRequestDto.getAmount());
        }

        userEntity.setAvailableBalance(userEntity.getAvailableBalance().add(amountValue));
        UserEntity updatedUserEntity = this.userRepo.save(userEntity);

        return new UserBalanceResponseDto(updatedUserEntity.getAvailableBalance(), updatedUserEntity.getExposure());

    }

    public void checkWithdrawPermission(UserEntity userEntity, BigDecimal amount) {

        if (amount.compareTo(userEntity.getAvailableBalance()) > 0) {
            throw new InsufficientFundsException();
        }

    }

    public UserBalanceResponseDto getUserBalance(UserEntity userEntity) {
        return new UserBalanceResponseDto(userEntity.getAvailableBalance(), userEntity.getExposure());
    }
}
