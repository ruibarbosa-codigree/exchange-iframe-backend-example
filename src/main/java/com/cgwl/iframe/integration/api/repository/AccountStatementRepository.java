package com.cgwl.iframe.integration.api.repository;

import com.cgwl.iframe.integration.api.entity.AccountStatementEntity;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface AccountStatementRepository extends JpaRepository<AccountStatementEntity, Long> {
    @Transactional
    void deleteByUserEntityAccountId(String accountId);

}
